<?php

include_once('vendor/autoload.php');

use Pondit\Calculator\Subtraction;
use Pondit\Calculator\Addition as A;
use Pondit\CHTG\Formatter;

$sum1 = new A("12345");
$formatter1 = new Formatter();
$formatter1->formatH1($sum1->serialNumber);
$subtraction1 = new Subtraction("232233");
$formatter1->formatPre($subtraction1->serialNumber);

//add 2 and 3.
$result = $sum1->add(2, 3);

//display the result using h1
$formatter1->formatH1($result);

//subtract 2 and 3.
$subtraction2 = new Subtraction("2343543");
$result = $subtraction2->subtract(2, 3);

//display the result using pre
$formatter2 = new Formatter();
$formatter2->formatPre($result);