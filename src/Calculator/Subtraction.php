<?php
namespace Pondit\Calculator;
class Subtraction
{
    public $serialNumber = null;

    public function __construct($serialNumber)
    {
        $this->serialNumber = "sub" . $serialNumber;
    }

    //declaration/defination of a method
    public function subtract($number1, $number2)
    {
        $result = $number1 - $number2;
        return $result;
    }
}