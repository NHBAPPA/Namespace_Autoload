<?php

namespace Pondit\CHTG;

class Formatter
{
    function formatPre($story)
    {
        echo '<pre>';
        echo $story;
        echo '</pre>';
        echo "<hr/>";
    }

    function formatH1($story)
    {
        echo '<h1>';
        echo $story;
        echo '</h1>';
        echo "<hr/>";
    }
}